import * as restify from 'restify'
import * as mongoose from 'mongoose'
import {environment} from '../config/environment'
import {Router} from '../common/router'
import {handleError} from './error.handler';

export class Server{

  application:restify.Server;

  initDB(){
    (<any>mongoose).Promise = global.Promise;
    return mongoose.connect(environment.db.url, {
      useMongoClient : true
    });
  }

  initRoutes(routers :Router[]): Promise<any>{
    return new Promise((resolve, reject)=> {
      try{
        this.application =  restify.createServer({
          name: 'lunch-time-api',
          version: '1.0.0'
        })

        this.application.use(restify.plugins.queryParser());
        this.application.use(restify.plugins.bodyParser());

        for(let router of routers){
          router.applyRouter(this.application);
        }

        this.application.listen(environment.server.port, ()=>{
          resolve(this.application)
        });

        this.application.on('restifyError', handleError)

      }catch{
        reject()
      }
    })
  }

  bootstrap(routers:Router[]): Promise<Server>{
    return this.initDB().then(()=>
           this.initRoutes(routers).then(() => this))
  }

  shutdown(){
    return mongoose.disconnect().then(()=>this.application.close());
  }
}
