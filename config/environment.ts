export const environment ={
  server: {
    port : process.env.SERVER_PORT || 3300,
  },
  db: {
    url: process.env.DB_URL || 'mongodb://localhost/lunch-time'
  },
  security: {
    saltRounds: process.env.SALT_ROUNDS || 10
  }
}
