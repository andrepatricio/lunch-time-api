import {Server} from './server/Server'
import {environment} from './config/environment'
import {usersRouter} from './users/users.router'
import {reviewsRouter} from './reviews/reviews.router'
import {restaurantsRouter} from './restaurants/restaurants.router'
import {User} from './users/users.model'
import {Review} from './reviews/reviews.model'
import {Restaurant} from './restaurants/restaurants.model'
import * as jestCli from 'jest-cli'

let server: Server

const beforeAllTests = () =>{
  environment.db.url = process.env.DB_URL || 'mongodb://localhost/lunch-time-test'
  environment.server.port = process.env.SERVER_PORT || 3301
  server = new Server()
  return server.bootstrap([usersRouter, reviewsRouter, restaurantsRouter])
        .then(()=>User.remove({}).exec())
        .then(()=>Review.remove({}).exec())
        .then(()=>Restaurant.remove({}).exec())
        .catch(console.error)
}

const afterAllTests = () => {
  return server.shutdown()
}

beforeAllTests()
  .then(()=>jestCli.run())
  .then(()=> afterAllTests())
  .catch(console.error)
