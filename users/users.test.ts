import 'jest'
import * as request from 'supertest'

let address: string = (<any>global).address

test('GET /users', ()=>{
  return request(address).get('/users').then(response =>{
    expect(response.status).toBe(200)
    expect(response.body.itens).toBeInstanceOf(Array)
  }).catch(fail)
})

test('GET /users/aaa', ()=>{
  return request(address).get('/users/aaa').then(response =>{
    expect(response.status).toBe(404)
  }).catch(fail)
})

test('POST /users', ()=>{
  let newUser = {
    name: "André",
    email: "andrepatricio@gmail.com",
    password: "test123"
  }
  return request(address)
            .post('/users').send(newUser).then(response =>{
              expect(response.status).toBe(200)
              expect(response.body.name).toBe(newUser.name)
              expect(response.body.email).toBe(newUser.email)
              expect(response.body.password).toBeUndefined()
              expect(response.body._id).toBeDefined()
            }).catch(fail)
})

test('PATCH /users/:id',()=>{
  return request(address)
            .post('/users')
            .send({
              name: "Arcenio",
              email: "arcenio@gmail.com",
              password: "test123"
            })
            .then(response => request(address)
                                .patch(`/users/${response.body._id}`)
                                .send({name: "Arcênio Patricio"})
                                .then(response => {
                                  expect(response.status).toBe(200)
                                  expect(response.body.name).toBe("Arcênio Patricio")
                                  expect(response.body._id).toBeDefined()
                                }))
            .catch(fail)
})

test('PATCH /users/aaaa - notfound',()=>{
  return request(address)
            .patch(`/users/aaaa`)
            .send({name: "Arcênio Patricio"})
            .then(response => {
              expect(response.status).toBe(404)
            }).catch(fail)
})

test('DELETE /users/:id',()=>{
  return request(address)
            .post('/users')
            .send({
              name: "Samira",
              email: "samira@gmail.com",
              password: "test123"
            })
            .then(response => request(address)
                                .delete(`/users/${response.body._id}`)
                                .then(response => {
                                  expect(response.status).toBe(204)
                                }))
            .catch(fail)
})

test('DELETE /users/aaaa - notfound',()=>{
  return request(address)
            .delete(`/users/aaa`)
            .then(response => {
                    expect(response.status).toBe(404)
            }).catch(fail)
})
