import * as mongoose from 'mongoose'
import * as bcrypt from 'bcrypt'
import {environment} from '../config/environment'

export interface User extends mongoose.Document{
  name: string,
  email: string,
  password: string
} 

export interface UserModel extends mongoose.Model<User>{
  findByEmail(email: string):Promise<User>
}

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Faltou o nome, cavalo!'],
    maxlength: [80, 'Nome gigante da porra, simplifica isso ai!'],
    minlength: [3, 'Ai não jovem, que porra de nome é esse']
  },
  email:{
    type: String,
    unique: true,
    required: [true, 'Email é obrigatorio, seu animal!']
  },
  password:{
    type: String,
    select: false,
    required: [true, 'Sem senha tu vai logar com o que, sua anta!']
  },
  gender:{
    type: String,
    required: false,
    enum: ['Male', 'Female']
  }
});

userSchema.statics.findByEmail = function(email){
  return this.findAll({email})
}

const hashPassword = function(obj, next) {
  bcrypt.hash(obj.password, environment.security.saltRounds)
        .then(hash => {
          obj.password = hash
          next()
        }).catch(next)
}

const saveMiddleware = function(next){
  const user: User = this
  if(!user.isModified('password')){
    next()
  } else {
    hashPassword(user, next)
  }
}
const updateMiddleware = function(next){
  if(!this.getUpdate().password){
    next()
  } else {
    hashPassword(this.getUpdate(), next)
  }
}

userSchema.pre('save', saveMiddleware)
userSchema.pre('findOneAndUpdate', updateMiddleware)
userSchema.pre('update', updateMiddleware)

export const User = mongoose.model<User, UserModel>('User', userSchema);
