import 'jest'
import * as request from 'supertest'

let address: string = (<any>global).address

test('GET /reviews', ()=>{
  return request(address).get('/reviews').then(response =>{
    expect(response.status).toBe(200)
  }).catch(fail)
})

test('POST /reviews - without restaurant and user', ()=>{
  return request(address)
          .post('/reviews')
          .send({
            rating: 3,
            comments: 'It could be better'
          }).then(response =>{
            expect(response.status).toBe(400)
          }).catch(fail)
})

test('POST /reviews - without restaurant', ()=>{
  return request(address)
          .post('/users')
          .send({
            name: 'reviewUser',
            email: 'review@user.com',
            password: 'test'
          }).then(response =>{
            request(address)
                    .post('/reviews')
                    .send({
                      rating: 3,
                      comments: 'It could be better',
                      user: response.body._id
                    }).then(response =>{
                      expect(response.status).toBe(404)
                    })
          }).catch(fail)
})
