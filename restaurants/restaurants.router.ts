import {ModelRouter} from '../common/model.router'
import * as restify from 'restify'
import {Restaurant} from './restaurants.model'

class RestaurantsRouter extends ModelRouter<Restaurant>{
  constructor(){
    super(Restaurant)
  }

  envelope(document:any):any{
    let resource = super.envelope(document)
    resource._links.menu = `${this.basePath}/${resource._id}/menu`
    return resource
  }

  findMenu = (req, resp, next) =>{
    Restaurant.findById(req.params.id, "+menu").then(restaurant=>{
      if(!restaurant){
        resp.json(404)
        return next()
      } else {
        resp.json(restaurant.menu)
        return next()
      }
    }).catch(next)
  }

  replaceMenu = (req, resp, next) =>{
    Restaurant.findById(req.params.id).then(restaurant=>{
      if(!restaurant){
        resp.json(404)
        return next()
      } else {
        restaurant.menu = req.body
        return restaurant.save()
      }
    }).then(restaurant=>{
      resp.json(restaurant.menu)
      return next()
    }).catch(next)
  }

  applyRouter(application: restify.Server){

    application.get(`${this.basePath}`, this.findAll);
    application.get(`${this.basePath}/:id`, [this.validateId, this.findById]);
    application.post(`${this.basePath}`, this.save);
    application.put(`${this.basePath}/:id`, [this.validateId, this.replace]);
    application.patch(`${this.basePath}/:id`, [this.validateId, this.update]);
    application.del(`${this.basePath}/:id`, [this.validateId, this.delete]);

    application.get(`${this.basePath}/:id/menu`, [this.validateId, this.findMenu])
    application.put(`${this.basePath}/:id/menu`, [this.validateId, this.replaceMenu])
  }
}

export const restaurantsRouter = new RestaurantsRouter()
