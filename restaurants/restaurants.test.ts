import 'jest'
import * as request from 'supertest'

let address: string = (<any>global).address

test('GET /restaurants', ()=>{
  return request(address).get('/restaurants').then(response =>{
    expect(response.status).toBe(200)
    expect(response.body.itens).toBeInstanceOf(Array)
  }).catch(fail)
})
